---
output:
  pdf_document: default
  html_document: default
---

ingy.biomass
============

The goal of ingy.biomass is to provide an R implementation of the component biomass equations developed by the Inland Northwest Growth and Yield Cooperative. The development and calibration of those equations is documented in [Affleck (2019; *Forest Ecology and Management* **432**: 179-188)](http://dx.doi.org/10.1016/j.foreco.2018.09.009). Equations exist for the following 7 conifer species

                Latin name Code
          Abies lasiocarpa ABLA
             Abies grandis ABGR
        Larix occidentalis LAOC
            Pinus contorta PICO
         Picea engelmannii PIEN
           Pinus ponderosa PIPO
     Pseudotsuga menziesii PSME

and for the following components

    dead_branches
    foliage
    live_branch_1hr
    live_branches
    crown
    stembark
    stemwood
    stem
    total_above_stump

Estimation of these biomass components can be made on the basis of 1. diameter at breast height (dbh; 1.37 m or 4.5 ft) only; 2. dbh and total height; or 3. dbh, total height, and live crown length. The package will choose the estimation basis from the predictor variables that are supplied. These predictor variables can be given in standard units (dbh in inches, heights and lengths in feet) in which case the mass components will be returned in pounds. This is the default mode. Alternatively, they can be given in metric units (dbh in cm, heights and lengths in m) in which the mass components will be returned in kg.

Installation
------------

The latest version of ingy.biomass is 0.1.0. It can be installed from a compressed binary file that is available from David Affleck (<david.affleck@umontana.edu>). Alternatively, it can be installed from its online BitBucket repository.
It is not available on CRAN.

Note: you'll need R version 3.5.0 or higher to run ingy.biomass.

### Installation from Binary

To install the ingy.biomass from its binary package file (ingy.biomass\_0.1.0.tar.gz) use:

    install.packages(file.path(my_dir,"ingy.biomass_0.1.0.tar.gz"),repos=NULL)

where my\_dir points to the directory into which you dropped the binary package file (e.g., my\_dir = "C:/Rstuff").

### Installation from BitBucket using Git & RStudio

RStudio has built-in [support for the version control software Git](https://support.rstudio.com/hc/en-us/articles/200532077-Version-Control-with-Git-and-SVN). If you work in RStudio and have Git configured on your machine, you can install ingy.biomass directly from its BitBucket repository. To do so, you'll need the remotes package, which can be installed from CRAN:

``` r
install.packages("remotes")
```

Then you can install the ingy.biomass package using

``` r
remotes::install_bitbucket("dlraffleck/ingybiomass",build_vignettes = TRUE) 
```

Vignette
--------

There is an expanded example of how to use this package that can be accessed via

``` r
library(ingy.biomass)
vignette("tree_biomass_estimation")
```

Brief Example
-------------

This is a basic example which shows you how to obtain biomass estimates for two 10'' trees, one Douglas-fir and one ponderosa pine:

``` r
library(ingy.biomass)
tree_biomass(tree_id=1:2,spp=c("PSME","PIPO"),
             dbh=rep(10,2)) # output in lbs
  tree_id mass_basis dead_branches  foliage live_branch_1hr live_branches
1       1          D      22.58588 37.96833       17.623608      79.91137
2       2          D      18.86957 30.93422        1.932258      78.38401
     crown stembark stemwood     stem total_above_stump
1 140.4656 50.81331 305.4089 356.2222          496.6878
2 128.1878 53.76058 275.3340 329.0946          457.2824
```

If heights and crown lengths were also available, then these could be supplied and improved estimates obtained:

``` r
tree_biomass(tree_id=1:2,spp=c("PSME","PIPO"),dbh=rep(10,2),ht=c(55,NA))
  tree_id mass_basis dead_branches  foliage live_branch_1hr live_branches
1       1         DH      22.58714 37.11255       17.923385      81.82167
2       2          D      18.86957 30.93422        1.932258      78.38401
     crown stembark stemwood     stem total_above_stump
1 141.5214 49.64150 299.9994 349.6409          491.1622
2 128.1878 53.76058 275.3340 329.0946          457.2824

tree_biomass(tree_id=1:2,spp=c("PSME","PIPO"),dbh=rep(10,2),ht=c(55,NA),cl=c(20,25))
  tree_id mass_basis dead_branches  foliage live_branch_1hr live_branches
1       1        DHC      22.42616 25.33125       13.684665      62.08944
2       2          D      18.86957 30.93422        1.932258      78.38401
     crown stembark stemwood     stem total_above_stump
1 109.8469 57.79890 299.3862 357.1851          467.0319
2 128.1878 53.76058 275.3340 329.0946          457.2824
```

The `mass_basis` attribute has changed in the output data.frames in these examples, reflecting the fact that height or height and crown length are now being used.

Finally, if metric inputs and outputs are desired, use the `IsMetric` input

``` r
tree_biomass(tree_id=1:2,spp=c("PSME","PIPO"),
             dbh=rep(25,2),ht=c(17.5,13.5),IsMetric=TRUE) #output in kg
  tree_id mass_basis dead_branches  foliage live_branch_1hr live_branches
1       1         DH      10.36097 15.28936        7.426990      33.64294
2       2         DH      10.45952 15.40249        1.024601      42.76278
     crown stembark  stemwood      stem total_above_stump
1 59.29327 23.22916 140.58990 163.81906          223.1123
2 68.62479 19.19285  80.62469  99.81754          168.4423
```
