% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/dataset_descriptions.R
\docType{data}
\name{basisD_mass}
\alias{basisD_mass}
\title{Functions for D-based estimation of biomass}
\format{A list with 7 species-specific elements, each of which
is a biomass estimation function.}
\usage{
basisD_mass
}
\description{
Functions for D-based estimation of biomass
}
\keyword{datasets}
