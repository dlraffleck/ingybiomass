# convert tree dimensions to/from metric units
#  x is a vector of numeric
#  IsMetric is a scalar Boolean describing the input units (and desired output units)

#' Convert a length from inches to centimeters
#'
#' @param x A numeric vector.
#' @param IsMetric A logical (TRUE/FALSE) value indicating whether \code{x} is in metric units.
#' @return The value of \code{x} in centimeters.
# #' @examples
# #' inch_to_cm(5,FALSE)
# #' inch_to_cm(5,TRUE)
inch_to_cm <- function(x,IsMetric){
  x <- x * ifelse(IsMetric, 1, 2.54)
  x
}

#' Convert a length from feet to meters
#'
#' @param x A numeric vector.
#' @param IsMetric A logical (TRUE/FALSE) value indicating whether \code{x} is in metric units.
#' @return The value of \code{x} in meters.
# #' @examples
# #' feet_to_m(5,FALSE)
# #' feet_to_m(5,TRUE)
feet_to_m <- function(x,IsMetric){
  x <- x * ifelse(IsMetric, 1, 0.3048)
  x
}

#' Convert a mass from kilograms to pounds
#'
#' @param x A numeric vector.
#' @param IsMetric A logical (TRUE/FALSE) value indicating whether the input tree dimensions are in metric or standard units.
#' @return The value of \code{x} in kilograms (when the input dimensions are metric) or pounds (when the input dimensions are in standard units).
# #' @examples
# #' kg_to_lbs(5,FALSE)
# #' kg_to_lbs(5,TRUE)
kg_to_lbs <- function(x,IsMetric){
  x <- x * ifelse(IsMetric, 1, 2.20462)
  x
}
