# creates the full list of tree dimensional input needed for mass estimation
#  need to pass equal vectors of tree identifiers and dimensions, plus metric yes/no
# returns data frame with all attributes needed for mass estimation

#' Compiles all variables that might be needed for biomass estimation. This
#' includes converting base dimensions to metric and/or NAs, then determining
#' the type of biomass estimation equation (D, DH, or DHC).  Logarithmic transforms
#' and principal components are also determined.
#'
#' @param tree_id A vector of unique tree identifiers (required)
#' @param spp A vector of tree species (required)
#' @param dbh A vector of tree diameters (required; in or cm)
#' @param ht A vector of tree heights (ft or m)
#' @param cl A vector of tree live crown lengths (ft or m)
#' @param IsMetric A logical (default=FALSE) indicating measurement unit system
#' (default: American standard units)
#' @return Dataframe with raw, logarithmic, and principal components of the
#' necessary predictor variables (all derived from metric forms).
tree_input_data <- function(tree_id,dbh,ht,cl,spp,IsMetric){
  out <- data.frame(tree_id,spp)
  # ensure metric
  out$DBH <- inch_to_cm(dbh,IsMetric)
  out$Height <- feet_to_m(ht,IsMetric)
  out$CL <- feet_to_m(cl,IsMetric)
  out$DBH <- ifelse(out$DBH<=0,NA,out$DBH)
  out$Height <- ifelse(out$Height<=0,NA,out$Height)
  out$CL <- ifelse(out$CL<=0,NA,out$CL)

  # find estimation bases
  out$mass_basis <- estimation_basis(out$DBH,out$Height,out$CL,out$spp)
  bases <- unique(out$mass_basis)

  # continue only if not all NA
  if( !(length(bases)==1 & is.na(bases[1])) ){
    # get log transforms
    out$ld <- log(out$DBH)
    out$lh <- log(out$Height)
    out$ll <- log(out$CL)

    # need dh PCs under some circumstances
    if (sum(nchar(bases)>=2,na.rm=TRUE)>0){
      pc1 <- merge(out,pc_dh_coeffs,by.x=c("spp"),by.y=c("code"),all.x=TRUE)
      pc1$dhpc1 <- with(pc1,ld_load*(ld-ld_cent)+lh_load*(lh-lh_cent))
      out <- merge(out,pc1[,c("tree_id","dhpc1")])
    }

    # need dhc PCs under some circumstances
    if (sum(nchar(bases)==3,na.rm=TRUE)>0){
      pc1 <- merge(out,pc_dhl_coeffs[pc_dhl_coeffs$pc==1,],by.x=c("spp"),by.y=c("code"),all.x=TRUE)
      pc1$dhlpc1 <- with(pc1,ld_load*(ld-ld_cent)+lh_load*(lh-lh_cent)+ll_load*(ll-ll_cent))
      out <- merge(out,pc1[,c("tree_id","dhlpc1")])

      pc1 <- merge(out,pc_dhl_coeffs[pc_dhl_coeffs$pc==2,],by.x=c("spp"),by.y=c("code"),all.x=TRUE)
      pc1$dhlpc2 <- with(pc1,ld_load*(ld-ld_cent)+lh_load*(lh-lh_cent)+ll_load*(ll-ll_cent))
      out <- merge(out,pc1[,c("tree_id","dhlpc2")])
    }
  }
  out
}


