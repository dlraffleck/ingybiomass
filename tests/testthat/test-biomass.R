test_that("match to supplementary material estimates", {
  case <- tree_biomass(232,"LAOC",30,25,15,IsMetric=TRUE)
  expect_equal(case$foliage, 6.413, tolerance = .001)
  expect_equal(case$dead_branches, 3.149, tolerance = .001)
  expect_equal(case$live_branches, 36.883, tolerance = .001)
  expect_equal(case$stembark, 36.299, tolerance = .001)
  expect_equal(case$stemwood, 328.051, tolerance = .001)
  expect_equal(case$live_branch_1hr, 9.818, tolerance = .001)
})

test_that("metric and imperial match", {
  case <- tree_biomass(232,"LAOC",30,25,15,IsMetric=TRUE)
  case2 <- tree_biomass(232,"LAOC",30/2.54,25/.3048,15/.3048,IsMetric=FALSE)
  expect_equal(case2$foliage, kg_to_lbs(case$foliage,F), tolerance = .001)
  expect_equal(case2$stembark, kg_to_lbs(case$stembark,F), tolerance = .001)
  expect_equal(case2$stemwood, kg_to_lbs(case$stemwood,F), tolerance = .001)
})





# path to analysis output files
pth <- "C:/DLRA/Projects/northwestbiomass"

set.seed(454)
tree.test <- data.frame()

# loop through species
spp_nms <- c("sf","gf","wl","pl","es","pp","df")
for (i in spp_nms){
  # load species stuff
  file_nm <- list.files(path=file.path(pth,"systems"),
                        pattern=paste("^",i,".2018may01.Rdata$",sep=""),
                        full.names = TRUE)
  load(file_nm)
  spp_code <- as.character(combo$latin[1])

  samp <- sample(1:nrow(combo2),20)

  tree.dim <- combo2[samp,c("FullID","DBH","Height","CL","comp")]
  tree.dim$spp <- spp_code
  tree.dim$donly <- donly$fitted[samp]
  tree.dim$dhonly <- dhonly$fitted[samp]
  tree.dim$nov <- nov$fitted[samp]

  tree.test <- rbind(tree.test,tree.dim)
}

tree.test <- tree.test[tree.test$comp %in% c("small","dead","foliage","live.branch",
                                             "drywood.bole","drybark.bole"),]
tree.test$tree_id <- 1:nrow(tree.test)
test_biom_dhc <- tree_biomass(tree_id=tree.test$tree_id,
                              spp=tree.test$spp,
                              dbh=tree.test$DBH,
                              ht=tree.test$Height,
                              cl=tree.test$CL,
                              IsMetric=TRUE)
test_biom_dh <- tree_biomass(tree_id=tree.test$tree_id,
                             spp=tree.test$spp,
                             dbh=tree.test$DBH,
                             ht=tree.test$Height,
                             IsMetric=TRUE)
test_biom_d <- tree_biomass(tree_id=tree.test$tree_id,
                            spp=tree.test$spp,
                            dbh=tree.test$DBH,
                            IsMetric=TRUE)
test1 <- merge(tree.test,test_biom_dhc,by.x="tree_id",by.y="tree_id")
test2 <- merge(tree.test,test_biom_dh,by.x="tree_id",by.y="tree_id")
test3 <- merge(tree.test,test_biom_d,by.x="tree_id",by.y="tree_id")

test_that("match DHC to dev datasets", {
  expect_equal(test1$foliage[test1$comp=="foliage"], test1$nov[test1$comp=="foliage"], tolerance = .001)
  expect_equal(test1$dead_branches[test1$comp=="dead"], test1$nov[test1$comp=="dead"], tolerance = .001)
  expect_equal(test1$live_branch_1hr[test1$comp=="small"], test1$nov[test1$comp=="small"], tolerance = .001)
  expect_equal(test1$live_branches[test1$comp=="live_branch"], test1$nov[test1$comp=="live_branch"], tolerance = .001)
  expect_equal(test1$stembark[test1$comp=="drybark.bole"], test1$nov[test1$comp=="drybark.bole"], tolerance = .001)
  expect_equal(test1$stemwood[test1$comp=="drywood.bole"], test1$nov[test1$comp=="drywood.bole"], tolerance = .001)
})

test_that("match DH to dev datasets", {
  expect_equal(test2$foliage[test2$comp=="foliage"], test2$dhonly[test2$comp=="foliage"], tolerance = .001)
  expect_equal(test2$dead_branches[test2$comp=="dead"], test2$dhonly[test2$comp=="dead"], tolerance = .001)
  expect_equal(test2$live_branch_1hr[test2$comp=="small"], test2$dhonly[test2$comp=="small"], tolerance = .001)
  expect_equal(test2$live_branches[test2$comp=="live_branch"], test2$dhonly[test2$comp=="live_branch"], tolerance = .001)
  expect_equal(test2$stembark[test2$comp=="drybark.bole"], test2$dhonly[test2$comp=="drybark.bole"], tolerance = .001)
  expect_equal(test2$stemwood[test2$comp=="drywood.bole"], test2$dhonly[test2$comp=="drywood.bole"], tolerance = .001)
})

test_that("match D to dev datasets", {
  expect_equal(test3$foliage[test3$comp=="foliage"], test3$donly[test3$comp=="foliage"], tolerance = .001)
  expect_equal(test3$dead_branches[test3$comp=="dead"], test3$donly[test3$comp=="dead"], tolerance = .001)
  expect_equal(test3$live_branch_1hr[test3$comp=="small"], test3$donly[test3$comp=="small"], tolerance = .001)
  expect_equal(test3$live_branches[test3$comp=="live_branch"], test3$donly[test3$comp=="live_branch"], tolerance = .001)
  expect_equal(test3$stembark[test3$comp=="drybark.bole"], test3$donly[test3$comp=="drybark.bole"], tolerance = .001)
  expect_equal(test3$stemwood[test3$comp=="drywood.bole"], test3$donly[test3$comp=="drywood.bole"], tolerance = .001)
})


# compr <- "foliage"
# compr <- "drywood.bole"
# compr <- "live.branch"
# compr <- "small"
# with(tree.test[tree.test$comp==compr,],plot(log(DBH),log(donly)))
# with(tree.test[tree.test$comp==compr,],points(log(DBH),log(dhonly),col="red"))
# with(tree.test[tree.test$comp==compr,],points(log(DBH),log(nov),col="green"))
# with(tree.test[tree.test$comp==compr,],plot(log(donly),log(dhonly),asp=1));abline(0,1)
